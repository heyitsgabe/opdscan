## sending notifications via text to recipient
from Web.Scanners.opdScan.error_logging import log_error
import re

## cell providers
## mint mobile uses t-mobile sms gateway
cp_l = ['verizon', 't-mobile', 'sprint', 'att', 'cirkcet', 'mint']

## messaging emails 
cp_m = ['@vtext.com', '@tmomail.net', '@messaging.sprintpcs.com', '@mms.att.net', 'mms.cricketwireless.net', '@tmomail.net']

## inputs
## cp = cell provider
## cn = cell number
cp = ''
cn = ''

## formats
cp_f = ''
cn_f = ''
c_sms = ''

## get cell providers
def get_cinfo():
    global cp
    global cn 

    cp = input('Enter your cell provider: ')
    cn = input('Enter your cell number (10 digits): ')

## formatting user inputs 
def f_in():
    global cp_f
    global cn_f

    cp_f = re.sub('\W', '', cp) ## removing non alphanumbers characters
    cn_f = re.sub('\D', '', cn) ## removing non digit characters

def chk_num(): ## confirming cell number = 10 digits
    if len(cn_f) != 10:
        print('Invalid number. Please re-enter your number.')
        get_cinfo()
    else:
        print('Valid phone number')

def find_cp(): ## matching provider input to providers in the list. 
    ci = 0
    
    global cp_f

    while ci <= len(cp_l):
        if cp_l[ci] != cp_f:
            print('match not found')
        else:
            print('match found')
            break
        ci += 1


## sending text message
def send_m():
    global cp_f
    global cn_f

    # try:
        
    # else:
    #     log_error()
    # ci += 1

