from datetime import datetime

fp = '/Users/gabriel/Documents/DevProjects/Web/Scanners/opdScan/ErrorLog/opd_scan_error.log'

def start_log():
    nt = datetime.now()
    et = nt.strftime('%m/%d/%Y %H:%M:%S')
    print('start logging at: ' + et)
    file_object = open(fp, 'a')
    file_object.write('\n' + str(et) + ' (start):------------------------------------------------------------')
    file_object.close()


def log_error():
    nt = datetime.now()
    et = nt.strftime('%m/%d/%Y %H:%M:%S')
    print('An error has been logged at: ' + et)
    file_object = open(fp, 'a')
    file_object.write('\n' + str(et) + ': ' + Exception)
    file_object.close()


def end_log():
    nt = datetime.now()
    et = nt.strftime('%m/%d/%Y %H:%M:%S')
    print('end logging at: ' + et)
    file_object = open(fp, 'a')
    file_object.write('\n' + str(et) + ' (end):---------------------------------------------------------------')
    file_object.close()